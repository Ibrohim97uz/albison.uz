const header = document.querySelector(".header");
const menuIcon = document.querySelector(".menu-icon");
const menuTable = document.querySelector(".nav-links-mobile");

document.addEventListener("scroll", () => {
  if (scrollY > 700) {
    menuTable.classList.remove("show");
  }
});
header.addEventListener("click", () => {
  menuTable.classList.remove("show");
});

menuIcon.addEventListener("click", () => menuTable.classList.toggle("show"));

const isNumericInput = (event) => {
  const key = event.keyCode;
  return (
    (key >= 48 && key <= 57) || // Allow number line
    (key >= 96 && key <= 105) // Allow number pad
  );
};

const isModifierKey = (event) => {
  const key = event.keyCode;
  return (
    event.shiftKey === true ||
    key === 35 ||
    key === 36 || // Allow Shift, Home, End
    key === 8 ||
    key === 9 ||
    key === 13 ||
    key === 46 || // Allow Backspace, Tab, Enter, Delete
    (key > 36 && key < 41) || // Allow left, up, right, down
    // Allow Ctrl/Command + A,C,V,X,Z
    ((event.ctrlKey === true || event.metaKey === true) &&
      (key === 65 || key === 67 || key === 86 || key === 88 || key === 90))
  );
};

const enforceFormat = (event) => {
  // Input must be of a valid number format or a modifier key, and not longer than ten digits
  if (!isNumericInput(event) && !isModifierKey(event)) {
    event.preventDefault();
  }
};

const formatToPhone = (event) => {
  if (isModifierKey(event)) {
    return;
  }

  const input = event.target.value.replace(/\D/g, "").substring(0, 12); // First ten digits of input only
  const areaCode = input.substring(3, 5);
  const middle = input.substring(5, 8);
  const last1 = input.substring(8, 10);
  const last2 = input.substring(10, 12);

  if (input.length > 10) {
    event.target.value = `+998 (${areaCode}) ${middle} - ${last1} - ${last2}`;
  } else if (input.length > 8) {
    event.target.value = `+998 (${areaCode}) ${middle} - ${last1}`;
  } else if (input.length > 5) {
    event.target.value = `+998 (${areaCode}) ${middle}`;
  } else if (input.length > 3) {
    event.target.value = `+998 (${areaCode}`;
  }
};

const inputElement = document.getElementById("phone");
inputElement.addEventListener("keydown", enforceFormat);
inputElement.addEventListener("keyup", formatToPhone);
