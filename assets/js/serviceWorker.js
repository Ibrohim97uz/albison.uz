const staticAlbisonAcademy = "albison-academy-site-v1";
const assets = [
  "/",
  "/index.html",
  "/thanks.html",
  "/ru/index.html",
  "/ru/thanks.html",
  "/assets/css/index.css",
  "/assets/css/bootstrap.css",
  "/assets/icons/adobe-xd.svg",
  "/assets/icons/adobeXD.svg",
  "/assets/icons/basic-englis.png",
  "/assets/icons/bootstrap.svg",
  "/assets/icons/branding.svg",
  "/assets/icons/conceptsOfDesign.svg",
  "/assets/icons/facebook.svg",
  "/assets/icons/favicon.svg",
  "/assets/icons/figma.svg",
  "/assets/icons/git.svg",
  "/assets/icons/gmail.svg",
  "/assets/icons/html.svg",
  "/assets/icons/instagram.svg",
  "/assets/icons/intro.svg",
  "/assets/icons/javascript.svg",
  "/assets/icons/listening-documents.png",
  "/assets/icons/npm.svg",
  "/assets/icons/react.svg",
  "/assets/icons/reading-documents.png",
  "/assets/icons/sass.svg",
  "/assets/icons/telegram.svg",
  "/assets/icons/tiktok-icon-2.svg",
  "/assets/icons/topic-english.png",
  "/assets/icons/userExperience.svg",
  "/assets/icons/userInterface.svg",
  "/assets/icons/world-list.png",
  "/assets/icons/youtube.svg",
  "/assets/images/aparto.svg",
  "/assets/images/ark-logistics.png",
  "/assets/images/beta.png",
  "/assets/images/dereksiya.svg",
  "/assets/images/logo-circle.png",
  "/assets/images/logo-little.png",
  "/assets/images/logotype.png",
  "/assets/images/MiTC-removebg-preview.png",
  "/assets/images/MiTC.png",
  "/assets/images/showcase-bg.png",
  "/assets/images/superiorities-bg.png",
  "/assets/images/veva.svg",
  "/assets/js/script.js",
  "/assets/js/serviceWorker.js",
  "/assets/manifest/icon-72x72.png",
  "/assets/manifest/icon-96x96.png",
  "/assets/manifest/icon-128x128.png",
  "/assets/manifest/icon-144x144.png",
  "/assets/manifest/icon-152x152.png",
  "/assets/manifest/icon-192x192.png",
  "/assets/manifest/icon-384x384.png",
  "/assets/manifest/icon-512x512.png",
];

self.addEventListener("install", (installEvent) => {
  installEvent.waitUntil(
    caches.open(staticAlbisonAcademy).then((cache) => {
      cache.addAll(assets);
    })
  );
});

self.addEventListener("fetch", (fetchEvent) => {
  fetchEvent.respondWith(
    caches.match(fetchEvent.request).then((res) => {
      return res || fetch(fetchEvent.request);
    })
  );
});

self.addEventListener("activate", function (event) {
  event.waitUntil(
    caches.keys().then(function (cacheNames) {
      return Promise.all(cacheNames.map(caches.delete));
    })
  );
});
